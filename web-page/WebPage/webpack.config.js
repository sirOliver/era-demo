const {merge} = require('webpack-merge');

const commonConfig = require('./build/webpack.common.js');

module.exports = ({mode, environment}) => {
    let conf = require(`./build/webpack.${mode}.js`)
    return merge(commonConfig, conf(environment));
};
