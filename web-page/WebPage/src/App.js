import "regenerator-runtime/runtime";
import React, {Fragment, Suspense} from 'react';
import {BrowserRouter, Routes, Route} from 'react-router-dom';
import FrontPage from "./main/FrontPage";

const ShopStep = React.lazy(() => import(/* webpackPrefetch: true */ "./shop/Step").then())
const ShopBoxStep = React.lazy(() => import(/* webpackPrefetch: true */ "./shop/1box").then())
const ShopCustomStep = React.lazy(() => import(/* webpackPrefetch: true */ "./shop/2custom").then())
const ShopFrequencyStep = React.lazy(() => import(/* webpackPrefetch: true */ "./shop/3frequency").then())

const App = () =>
    <BrowserRouter>
        <Suspense fallback={<Fragment/>}>
            <Routes>
                <Route path="/" element={<FrontPage/>}/>
                <Route element={<ShopStep/>}>
                    <Route path="/shop/box" element={<ShopBoxStep/>}/>
                    <Route path="/shop/custom" element={<ShopCustomStep/>}/>
                    <Route path="/shop/frequency" element={<ShopFrequencyStep/>}/>
                </Route>
            </Routes>
        </Suspense>
    </BrowserRouter>

export default App