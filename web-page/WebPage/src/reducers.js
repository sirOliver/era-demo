import {combineReducers} from 'redux'
import shopReducer from './shop/shopSlice'

export default combineReducers({
    shop: shopReducer
})