import React from "react";
import PropTypes from "prop-types";
import Style from './button.module.scss';

export const ButtonCategory = {
    SHOP_OPTION_UNSELECTED: 1,
    SHOP_OPTION_SELECTED: 2,
    SHOP_NEXT: 3,
    SHOP_SKIP: 4,
    SHOP_SEARCH: 5,
    SHOP_BACK: 6,
    SHOP_ITEM_HANDLER: 7,
    FRONT_CALL: 11,
    FRONT_SHOP: 12,
    FRONT_FAQ: 13
}

const CategoryClass = (() => {
    let map = new Map()
    map.set(ButtonCategory.SHOP_OPTION_UNSELECTED, `${Style.button} ${Style.rounded} ${Style.option} ${Style.whiteGreen} ${Style.whiteGreenUnselected}`)
    map.set(ButtonCategory.SHOP_OPTION_SELECTED, `${Style.button} ${Style.rounded} ${Style.option} ${Style.whiteGreen} ${Style.whiteGreenSelected}`)
    map.set(ButtonCategory.SHOP_NEXT, `${Style.button} ${Style.rounded} ${Style.action} ${Style.green}`)
    map.set(ButtonCategory.SHOP_SKIP, `${Style.button} ${Style.rounded} ${Style.action} ${Style.black}`)
    map.set(ButtonCategory.SHOP_SEARCH, `${Style.button} ${Style.search} ${Style.green}`)
    map.set(ButtonCategory.SHOP_BACK, `${Style.button} ${Style.back}`)
    map.set(ButtonCategory.SHOP_ITEM_HANDLER, `${Style.button} ${Style.itemHandler}`)
    map.set(ButtonCategory.FRONT_CALL, `${Style.button} ${Style.rounded} ${Style.call} ${Style.black}`)
    map.set(ButtonCategory.FRONT_SHOP, `${Style.button} ${Style.rounded} ${Style.shop} ${Style.green}`)
    map.set(ButtonCategory.FRONT_FAQ, `${Style.button} ${Style.itemHandler}`)
    return map
})()

const Button = ({category, text, action, disabled}) =>
    <button type="button" className={CategoryClass.get(category)} dangerouslySetInnerHTML={{__html: text}}
            onClick={action} disabled={disabled}/>

Button.propTypes = {
    category: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    action: PropTypes.func,
    disabled: PropTypes.bool
}

export default Button