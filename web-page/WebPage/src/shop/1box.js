import React, {Fragment, useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {updateBoxSize, updateStep} from "./shopSlice";
import {useNavigate} from "react-router-dom";
import Button, {ButtonCategory} from "../primitives/Button";
import {s} from "../languages/en-shop";
import StyleBox from './box.module.scss';
import StyleStep from './step.module.scss';

const exampleOffers = [
    {
        price: 1.27,
        name: "Jutetee",
        quantity: 2
    },
    {
        price: 1.2,
        name: "Toothpaste Tablets—with Fluoride",
        quantity: 1
    }
] //todo replace with dynamic data

const Offer = () => {
    return (
        <div className={`${StyleStep.main} ${StyleStep.scroller}`}>
            <ul className={StyleBox.offers}>
                {exampleOffers.map((value, index) => {
                    return (
                        <li key={index}>
                            <div className={StyleBox.offer}>
                                <span className={StyleStep.bold}>&euro;{value.price.toFixed(2)}</span>
                                <span>{s.quantityShort + value.quantity}</span>
                            </div>
                            {value.name}
                        </li>
                    )
                })}
            </ul>
        </div>
    )
}

const Box = () => {
    const boxSize = useSelector(state => state.shop.boxSize)
    const dispatch = useDispatch()

    const navigate = useNavigate()

    useEffect(() => {
        dispatch(updateStep(1))
        if (boxSize === 0)
            dispatch(updateBoxSize(1))
    }, [])

    return (
        <Fragment>
            <ul className={StyleStep.options}>
                <li>
                    <Button
                        category={boxSize === 1 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.small} action={() => dispatch(updateBoxSize(1))} disabled={boxSize === 1}/>
                </li>
                <li>
                    <Button
                        category={boxSize === 2 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.medium} action={() => dispatch(updateBoxSize(2))} disabled={boxSize === 2}/>
                </li>
                <li>
                    <Button
                        category={boxSize === 3 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.large} action={() => dispatch(updateBoxSize(3))} disabled={boxSize === 3}/>
                </li>
            </ul>
            <Offer/>
            <ul className={StyleBox.actions}>
                <li>
                    <Button category={ButtonCategory.SHOP_NEXT} text={s.surpriseMe}
                            action={() => {
                            }}/>
                </li>
                <li>
                    <Button category={ButtonCategory.SHOP_SKIP} text={s.customise}
                            action={() => navigate("/shop/custom")}/>
                </li>
            </ul>
        </Fragment>
    )
}

export default Box