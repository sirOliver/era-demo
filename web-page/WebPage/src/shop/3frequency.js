import React, {Fragment, useEffect} from 'react';
import {updateFrequency, updateStep} from "./shopSlice";
import {useDispatch, useSelector} from "react-redux";
import Button, {ButtonCategory} from "../primitives/Button";
import {s} from "../languages/en-shop";
import {useNavigate} from "react-router-dom";
import StyleStep from "./step.module.scss";

const Frequency = () => {
    const frequency = useSelector(state => state.shop.frequency)
    const dispatch = useDispatch()

    const navigate = useNavigate()

    useEffect(() => {
        dispatch(updateStep(3))
    }, [])

    return (
        <Fragment>
            <ul className={`${StyleStep.options} ${StyleStep.scroller}`}>
                <li>
                    <Button
                        category={frequency === 0 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.onlyOnce} action={() => dispatch(updateFrequency(0))} disabled={frequency === 0}/>
                </li>
                <li>
                    <Button
                        category={frequency === 1 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.onceWeek} action={() => dispatch(updateFrequency(1))} disabled={frequency === 1}/>
                </li>
                <li>
                    <Button
                        category={frequency === 2 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.twiceWeek} action={() => dispatch(updateFrequency(2))} disabled={frequency === 2}/>
                </li>
                <li>
                    <Button
                        category={frequency === 3 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.onceMonth} action={() => dispatch(updateFrequency(3))} disabled={frequency === 3}/>
                </li>
                <li>
                    <Button
                        category={frequency === 4 ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                        text={s.twiceMonth} action={() => dispatch(updateFrequency(4))} disabled={frequency === 4}/>
                </li>
            </ul>
            <div className={`${StyleStep.main} ${StyleStep.scroller}`}/>
            <div className={StyleStep.actions}>
                <Button category={ButtonCategory.SHOP_BACK} text="" action={() => history.go(-1)}/>
                <Button category={ButtonCategory.SHOP_NEXT} text={s.boxReview} action={() => navigate("/checkout")}/>
            </div>
        </Fragment>
    )
}

export default Frequency