import React, {Fragment, useEffect, useState} from 'react';
import {renderToString} from 'react-dom/server';
import StyleCustom from "./custom.module.scss";
import StyleStep from './step.module.scss';
import {updateBoxSize, updateCategories, updateItems, updateStep} from "./shopSlice";
import {useDispatch, useSelector} from "react-redux";
import Button, {ButtonCategory} from "../primitives/Button";
import {s} from "../languages/en-shop";
import PropTypes from "prop-types";
import minus from '../images/minus.svg';
import plus from '../images/plus.svg';
import {useNavigate} from "react-router-dom";

const Search = () => {
    const savedQuery = useSelector(state => state.shop.query)
    const dispatch = useDispatch()

    const [query, setQuery] = useState(savedQuery)

    return (
        <div className={StyleCustom.searchBox}>
            <input type="text" placeholder={s.searchPlaceholder} name="query" onChange={e => setQuery(e.target.value)}
                   value={String(query)} className={StyleCustom.searchInput}/>
            <Button category={ButtonCategory.SHOP_SEARCH} text={s.search}/>
        </div>
    )
}

const hardcodedCategories = ["Home-Care", "Cereals & Grains", "Condiments", "Coffee & Tea", "Dry/Baking Goods", "Diary, Eggs & Meat", "Pet", "Seeds & Nuts", "Beverages", "Self-Care"] //todo replace with dynamic data

const Categories = () => {
    const categories = useSelector(state => state.shop.categories)
    const dispatch = useDispatch()

    const [content, setContent] = useState(<Fragment/>)

    useEffect(() => {
        console.log(categories)
        setContent(
            <ul className={`${StyleStep.options} ${StyleStep.scroller}`}>
                {hardcodedCategories.map((value, index) => {
                    return (
                        <li key={`cat-${index}`}>
                            <Button
                                category={categories.includes(value) ? ButtonCategory.SHOP_OPTION_SELECTED : ButtonCategory.SHOP_OPTION_UNSELECTED}
                                text={value} action={() => dispatch(updateCategories({
                                category: value,
                                selected: !categories.includes(value)
                            }))}/>
                        </li>
                    )
                })}
            </ul>
        )
    }, [categories])

    return content
}

const Adder = ({selection, plusAction, minusAction}) => {
    const [content, setContent] = useState(<Fragment/>)

    useEffect(() => {
        if (selection > 0) {
            setContent(
                <div className={StyleCustom.itemHandlerWrapper}>
                    <div className={`${StyleCustom.itemHandlerContainer} ${StyleCustom.removerAndAdder}`}>
                        <Button category={ButtonCategory.SHOP_ITEM_HANDLER} action={minusAction}
                                text={renderToString(<img src={minus} alt="-" width="32" height="32"/>)}/>
                        <div className={StyleCustom.selectionContainer}>
                            <span className={StyleStep.bold}>{selection}</span>
                        </div>
                        <Button category={ButtonCategory.SHOP_ITEM_HANDLER} action={plusAction}
                                text={renderToString(<img src={plus} alt="+" width="32" height="32"/>)}/>
                    </div>
                </div>
            )
        } else {
            setContent(
                <div className={StyleCustom.itemHandlerWrapper}>
                    <div className={`${StyleCustom.itemHandlerContainer} ${StyleCustom.adder}`}>
                        <Button category={ButtonCategory.SHOP_ITEM_HANDLER} action={plusAction}
                                text={renderToString(<img src={plus} alt="+" width="32" height="32"/>)}/>
                    </div>
                </div>
            )
        }
    }, [selection])

    return content
}

Adder.propTypes = {
    selection: PropTypes.number.isRequired,
    plusAction: PropTypes.func.isRequired,
    minusAction: PropTypes.func.isRequired
}

const Items = () => {
    const items = useSelector(state => state.shop.items)
    const dispatch = useDispatch()

    const [content, setContent] = useState(<Fragment/>)

    useEffect(() => {
        setContent(
            <div className={`${StyleCustom.items} ${StyleStep.scroller}`}>
                {items.map((value, index) => {
                    return (
                        <div key={`item-${index}`} className={StyleCustom.item}>
                            <div className={StyleCustom.card}>
                                <Adder selection={value.selection}
                                       plusAction={() => {
                                           //todo items do not get updated, this should be fixed during integration
                                           let newItems = JSON.parse(JSON.stringify(items))
                                           newItems[index].selection = newItems[index].selection + 1
                                           dispatch(updateItems(newItems))
                                       }}
                                       minusAction={() => {
                                           let newItems = JSON.parse(JSON.stringify(items))
                                           newItems[index].selection = newItems[index].selection - 1
                                           dispatch(updateItems(newItems))
                                       }}/>
                                <div className={StyleCustom.imgContainer}>
                                    <div className={StyleCustom.imgWrapper}>
                                        <img src={value.image} alt="Product Image" height="10" width="10"
                                             className={StyleCustom.img}/>
                                    </div>
                                </div>
                                <span className={StyleStep.bold}>&euro;{value.price.toFixed(2)}</span>
                                <br/>
                                <span>{value.name}</span>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }, [items])

    return content
}

const Custom = () => {
    const dispatch = useDispatch()

    const navigate = useNavigate()

    useEffect(() => {
        dispatch(updateStep(2))
        dispatch(updateBoxSize(0))
    }, [])

    return (
        <Fragment>
            <Search/>
            <Categories/>
            <Items/>
            <div className={StyleStep.actions}>
                <Button category={ButtonCategory.SHOP_BACK} text="" action={() => history.go(-1)}/>
                <div className={StyleCustom.bottomRight}>
                    <div className={StyleCustom.subTotal}>
                        {s.subTotal}
                        <br/>
                        <span className={StyleStep.bold}>&euro;30.43</span>
                    </div>
                    <Button category={ButtonCategory.SHOP_NEXT} text={s.frequencyTitle} action={() => navigate("/shop/frequency")}/>
                </div>
            </div>
        </Fragment>
    )
}

export default Custom