import React, {useEffect, useState} from 'react';
import {Outlet} from "react-router-dom";
import {useSelector} from "react-redux";
import {s} from "../languages/en-shop";
import Style from './step.module.scss';

const Step = () => {
    const step = useSelector(state => state.shop.step)

    const [content, setContent] = useState(<Outlet/>)

    useEffect(() => {
        let title = ""
        let paragraph = ""
        switch (step) {
            case 1:
                title = s.chooseBoxTitle
                paragraph = s.chooseBoxParagraph
                break
            case 2:
                title = s.addProductsTitle
                paragraph = s.addProductsParagraph
                break
            case 3:
                title = s.frequencyTitle
                paragraph = s.frequencyParagraph
                break
            default:
                setContent(<Outlet/>)
                return
        }
        setContent(
            <div className={Style.step}>
                <div className={Style.header}>
                    <div>
                        <h3 className={Style.h3}>{title}</h3>
                        <p className={Style.p}>{paragraph}</p>
                    </div>
                    <div>
                        <ul className={Style.progress}>
                            <li>
                                <div className={`${Style.circle} ${step === 1 ? Style.active : ""}`}/>
                            </li>
                            <li>
                                <div className={`${Style.circle} ${step === 2 ? Style.active : ""}`}/>
                            </li>
                            <li>
                                <div className={`${Style.circle} ${step === 3 ? Style.active : ""}`}/>
                            </li>
                        </ul>
                    </div>
                </div>
                <Outlet/>
            </div>
        )
    }, [step])

    return content
}

export default Step