import {createSlice} from '@reduxjs/toolkit'

const emptyState = {
    step: 0,
    boxSize: 0,
    categories: [],
    items: [
        {
            price: 1.27,
            name: "Jutetee",
            image: "",
            selection: 0
        },
        {
            price: 17,
            name: "Menstrual Cups S",
            image: "https://media.istockphoto.com/photos/lioness-displaing-dangerous-teeth-picture-id153499239",
            selection: 0
        },
        {
            price: 1.26,
            name: "Olive oil",
            image: "",
            selection: 0
        },
        {
            price: .69,
            name: "Mountain lentils",
            image: "https://media.istockphoto.com/photos/horse-with-tongue-stuck-out-picture-id1192159893",
            selection: 0
        },
        {
            price: 1.55,
            name: "Crunched walnuts",
            image: "https://media.istockphoto.com/photos/snowy-egret-on-sanibel-island-beach-picture-id1218105485",
            selection: 0
        },
        {
            price: 1.2,
            name: "Organic brown almonds long long long",
            image: "https://as1.ftcdn.net/v2/jpg/02/01/30/82/1000_F_201308289_GP6YkLs0rfIJjPJIG05GsvHLR7r1YSGe.jpg",
            selection: 0
        },
        {
            price: 1.2,
            name: "Organic brown almonds",
            image: "",
            selection: 0
        },
        {
            price: 17,
            name: "Menstrual Cups S",
            image: "",
            selection: 0
        },
        {
            price: 1.26,
            name: "Olive oil",
            image: "",
            selection: 0
        },
        {
            price: .69,
            name: "Mountain lentils",
            image: "",
            selection: 0
        },
        {
            price: 1.55,
            name: "Crunched walnuts",
            image: "",
            selection: 0
        },
        {
            price: 1.2,
            name: "Organic brown almonds",
            image: "",
            selection: 0
        },
        {
            price: 1.2,
            name: "Organic brown almonds",
            image: "",
            selection: 0
        }
    ], //todo replace with dynamic data
    query: "",
    frequency: 0
}

const shopSlice = createSlice({
    name: 'shop',
    initialState: emptyState,
    reducers: {
        updateStep(state, action) {
            state.step = action.payload
        },
        updateBoxSize(state, action) {
            state.boxSize = action.payload
        },
        updateCategories(state, action) {
            if (action.payload.selected) {
                state.categories.push(action.payload.category)
            } else {
                state.categories = state.categories.filter(v => {
                    return v !== action.payload.category
                })
            }
            state.selectedPage = 0
        },
        updateItems(state, action) {
            state.items = action.payload
        },
        updateQuery(state, action) {
            state.query = action.payload
            state.selectedPage = 0
        },
        updateFrequency(state, action) {
            state.frequency = action.payload
        }
    }
})

export const {
    updateStep,
    updateBoxSize,
    updateCategories,
    updateItems,
    updateQuery,
    updateFrequency
} = shopSlice.actions

export default shopSlice.reducer