import React from 'react';
import Style from "./footer.module.scss";
import logo from '../images/logo_green.png';
import {s} from "../languages/en-front";
import {Link} from "react-router-dom";
import facebook from '../images/facebook.png';
import twitter from '../images/twitter.png';
import instagram from '../images/instagram.png';

const Footer = () => {
    return (
        <footer className={Style.background}>
            <div className={Style.wrapper}>
                <div className={Style.main}>
                    <div className={Style.era}>
                        <img src={logo} alt="Era Zero Waste" width="160" height="78"/>
                        <p className={Style.paragraph}>{s.era}</p>
                        <p className={Style.paragraph}>{s.weeklyGroceries}</p>
                    </div>
                    <div className={Style.grid}>
                        <div>
                            <h3 className={Style.group}>
                                {s.information}
                            </h3>
                            <Link to="/about-us">
                                <span className={Style.link}>{s.aboutUs}</span>
                            </Link>
                            <Link to="/sustainability">
                                <span className={Style.link}>{s.sustainabilityFirstCap}</span>
                            </Link>
                            <Link to="/impressum">
                                <span className={Style.link}>{s.impressum}</span>
                            </Link>
                            <Link to="/faq">
                                <span className={Style.link}>{s.faqShort}</span>
                            </Link>
                        </div>
                        <div>
                            <h3 className={Style.group}>
                                {s.myAccount}
                            </h3>
                            <Link to="/impact">
                                {/*todo add missing link*/}
                                <span className={Style.link}>{s.knowImpact}</span>
                            </Link>
                            <Link to="/orders">
                                <span className={Style.link}>{s.orderHistory}</span>
                            </Link>
                            <Link to="/privacy">
                                <span className={Style.link}>{s.privacy}</span>
                            </Link>
                            <Link to="/legal">
                                <span className={Style.link}>{s.terms}</span>
                            </Link>
                        </div>
                        <div>
                            <h3 className={Style.group}>
                                {s.sellers}
                            </h3>
                            <Link to="/sellers/become">
                                <span className={Style.link}>{s.becomeSeller}</span>
                            </Link>
                            <Link to="/sellers/refer">
                                <span className={Style.link}>{s.referSeller}</span>
                            </Link>
                        </div>
                        <div>
                            <h3 className={Style.group}>
                                {s.socials}
                            </h3>
                            <Link to="/contact-us">
                                <span className={Style.link}>{s.contactUs}</span>
                            </Link>
                        </div>
                    </div>
                </div>
                <div className={Style.copyright}>
                    <span>{s.copyright}</span>
                    <div className={Style.channels}>
                        <a target="_blank" rel="nofollow noopener noreferrer"
                           href="https://www.facebook.com/zerowasteera">
                            <div className={Style.channel}>
                                <img src={facebook} alt="Facebook" width="10" height="10" className={Style.img}/>
                            </div>
                        </a>
                        <a target="_blank" rel="nofollow noopener noreferrer"
                           href="https://twitter.com/erazerowaste">
                            <div className={Style.channel}>
                                <img src={twitter} alt="Twitter" width="10" height="10" className={Style.img}/>
                            </div>
                        </a>
                        <a target="_blank" rel="nofollow noopener noreferrer"
                           href="https://www.instagram.com/erazerowaste/">
                            <div className={Style.channel}>
                                <img src={instagram} alt="Instagram" width="10" height="10" className={Style.img}/>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </footer>
    )
}

export default Footer