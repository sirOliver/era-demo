import React from 'react';
import Style from "./header.module.scss";
import logo from '../images/logo_black.png';

const Header = () => {
    return (
        <header className={Style.background}>
            <span className={Style.helper}/>
            <img src={logo} alt="Logo" width="12" height="6" className={Style.logo}/>
        </header>
    )
}

export default Header