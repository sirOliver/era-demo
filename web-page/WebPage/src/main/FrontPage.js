import React, {Fragment, useState} from 'react';
import Footer from "./Footer";
import Header from "./Header";
import Style from "./front.module.scss";
import {s as sFront} from "../languages/en-front";
import {s as sFaq} from "../languages/en-faq";
import PropTypes from "prop-types";
import groceries_1 from '../images/groceries_1.jpeg';
import box_example_1 from '../images/box_example_1.jpeg';
import delivery_example_1 from '../images/delivery_example_1.jpeg';
import marketplace from '../images/marketplace.jpeg';
import groceries_2 from '../images/groceries_2.jpeg';
import box_example_2 from '../images/box_example_2.jpeg';
import delivery_example_2 from '../images/delivery_example_2.jpeg';
import sifted from '../images/sifted.jpeg';
import business_insider from '../images/business_insider.jpeg';
import market_watch from '../images/market_watch.jpeg';
import yahoo_finance from '../images/yahoo_finance.jpeg';
import eyewitness_news from '../images/eyewitness_news.jpeg';
import trendhunter from '../images/trendhunter.jpeg';
import cosmetics_business from '../images/cosmetics_business.jpeg';
import un_8 from '../images/un_8.jpeg';
import un_11 from '../images/un_11.jpeg';
import un_12 from '../images/un_12.jpeg';
import un_13 from '../images/un_13.jpeg';
import Button, {ButtonCategory} from "../primitives/Button";
import {renderToString} from "react-dom/server";
import minus from "../images/minus.svg";
import plus from "../images/plus.svg";

const Ad = ({imageSrc, imageAlt, title, text}) => {
    return (
        <div>
            <img src={imageSrc} alt={imageAlt} width="14" height="9" className={Style.adImage}/>
            <h4 className={Style.adTitle}>{title}</h4>
            <span className={Style.adText}>{text}</span>
        </div>
    )
}

Ad.propTypes = {
    imageSrc: PropTypes.string.isRequired,
    imageAlt: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
}

const Note = ({imageSrc, imageAlt, title, text, button, textOnRight}) => {
    const imagePart = <img src={imageSrc} alt={imageAlt} width="33" height="22"
                           className={`${Style.noteImage} ${textOnRight ? Style.noteRightMargin : Style.noteLeftMargin}`}/>
    const textPart = <div><h4 className={Style.noteTitle}>{title}</h4>
        <span className={Style.noteText}>{text}</span>{button}</div>

    return (
        <div className={Style.note}>
            {textOnRight ?
                <Fragment>{imagePart}{textPart}</Fragment>
                :
                <Fragment>{textPart}{imagePart}</Fragment>
            }
        </div>
    )
}

Note.propTypes = {
    imageSrc: PropTypes.string.isRequired,
    imageAlt: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
    button: PropTypes.object.isRequired,
    textOnRight: PropTypes.bool
}

const Question = ({question, answers}) => {
    const [opened, setOpened] = useState(false)

    return (
        <div className={Style.question}>
            <div className={Style.questionTitle}>
                <span className={Style.questionTitleText}>{question}</span>
                <Button category={ButtonCategory.FRONT_FAQ} action={() => setOpened(!opened)}
                        text={opened ?
                            renderToString(<img src={minus} alt="-" width="32" height="32"/>)
                            :
                            renderToString(<img src={plus} alt="+" width="32" height="32"/>)}/>
            </div>
            {opened ? answers : ""}
        </div>
    )
}

Question.propTypes = {
    question: PropTypes.string.isRequired,
    answers: PropTypes.object.isRequired
}

const FrontPage = () => {
    return (
        <Fragment>
            <Header/>
            <div>
                <div className={Style.titleWrapper}>
                    <h1 className={Style.title}>{sFront.title}</h1>
                    <h2 className={Style.slogan}>{sFront.slogan}</h2>
                    <div className={Style.titleActionWrapper}>
                        <Button category={ButtonCategory.FRONT_CALL} text={sFront.customizeBoxNow}
                                action={() => window.location.href = "/shop/box"}/>
                    </div>
                </div>
                <div className={Style.ads}>
                    <h2 className={Style.adsTitle}>{sFront.weeklyGroceries}</h2>
                    <div className={Style.adsWrapper}>
                        <Ad imageSrc={groceries_1} imageAlt="Groceries"
                            title={sFront.customizeBoxTitle} text={sFront.customizeBoxText}/>
                        <Ad imageSrc={box_example_1} imageAlt="Box example"
                            title={sFront.neverRunOutTitle} text={sFront.neverRunOutText}/>
                        <Ad imageSrc={delivery_example_1} imageAlt="Delivery example"
                            title={sFront.makeDifferenceTitle} text={sFront.makeDifferenceText}/>
                    </div>
                    <div className={Style.adsActionWrapper}>
                        <Button category={ButtonCategory.FRONT_CALL} text={sFront.getStarted}
                                action={() => window.location.href = "/shop/box"}/>
                    </div>
                </div>
                <div className={Style.notes}>
                    <Note button={<Button category={ButtonCategory.FRONT_SHOP} text={sFront.exploreProducts}
                                          action={() => window.location.href = "/shop/box"}/>}
                          text={sFront.productsText} title={sFront.productsTitle}
                          imageSrc={marketplace} imageAlt="Marketplace" textOnRight={true}/>
                    <Note button={<Button category={ButtonCategory.FRONT_SHOP} text={sFront.customizeBox}
                                          action={() => window.location.href = "/shop/box"}/>}
                          text={sFront.savingsText} title={sFront.savingsTitle}
                          imageSrc={groceries_2} imageAlt="Groceries"/>
                    <Note button={<Button category={ButtonCategory.FRONT_SHOP} text={sFront.seeBoxes}
                                          action={() => window.location.href = "/shop/box"}/>}
                          text={sFront.orderText} title={sFront.orderTitle}
                          imageSrc={box_example_2} imageAlt="Box example" textOnRight={true}/>
                    <div className={Style.brands}>
                        <span className={Style.thinText}>{sFront.asSeenOn}</span>
                        <div>
                            <img src={sifted} alt="Sifted" width="13" height="11"
                                 className={Style.brandImage}/>
                            <img src={business_insider} alt="Business Insider" width="13" height="11"
                                 className={Style.brandImage}/>
                            <img src={market_watch} alt="MarketWatch" width="13" height="11"
                                 className={Style.brandImage}/>
                            <img src={yahoo_finance} alt="Yahoo! Finance" width="13" height="11"
                                 className={Style.brandImage}/>
                            <img src={eyewitness_news} alt="3 Eyewitness News" width="13" height="11"
                                 className={Style.brandImage}/>
                            <img src={trendhunter} alt="Trendhunter" width="13" height="11"
                                 className={Style.brandImage}/>
                            <img src={cosmetics_business} alt="Cosmetics Business" width="13" height="11"
                                 className={Style.brandImage}/>
                        </div>
                    </div>
                    <Note button={<Button category={ButtonCategory.FRONT_SHOP} text={sFront.sustainabilityLarge}
                                          action={() => window.location.href = "/shop/box"}/>}
                          text={sFront.differenceText} title={sFront.differenceTitle}
                          imageSrc={delivery_example_2} imageAlt="Delivery example"/>
                    <div className={Style.goals}>
                        <span className={Style.thinText}>{sFront.unGoals}</span>
                        <div className={Style.goalIcons}>
                            <img src={un_8} alt="Decent work and economic growth" width="11" height="11"
                                 className={Style.brandImage}/>
                            <img src={un_11} alt="Sustainable cities and communities" width="11" height="11"
                                 className={Style.brandImage}/>
                            <img src={un_12} alt="Responsible consumption and production" width="11" height="11"
                                 className={Style.brandImage}/>
                            <img src={un_13} alt="Climate action" width="11" height="11"
                                 className={Style.brandImage}/>
                        </div>
                    </div>
                    <div className={Style.faqs}>
                        <span className={Style.thinText}>{sFront.faqLong}</span>
                        <div className={Style.questions}>
                            <Question question={sFaq.depositTitle} answers={<p>{sFaq.depositText}</p>}/>
                            <Question question={sFaq.deliveryOneTitle} answers={<p>{sFaq.deliveryOneText}</p>}/>
                            <Question question={sFaq.deliveryTwoTitle} answers={<p>{sFaq.deliveryTwoText}</p>}/>
                            <Question question={sFaq.deliveryThreeTitle} answers={<p>{sFaq.deliveryThreeText}</p>}/>
                            <Question question={sFaq.specialTitle} answers={<p>{sFaq.specialText}</p>}/>
                            <Question question={sFaq.sustainabilityTitle} answers={
                                <Fragment>
                                    <p>{sFaq.sustainabilityText1}</p>
                                    <p>{sFaq.sustainabilityText2}</p>
                                    <p>{sFaq.sustainabilityText3}</p>
                                    <p>{sFaq.sustainabilityText4}</p>
                                    <p>{sFaq.sustainabilityText5}</p>
                                </Fragment>
                            }/>
                            {/*    todo add others */}
                        </div>
                    </div>
                    <div className={Style.conclusions}>
                        <span className={Style.thinText}>{sFront.conclusion}</span>
                        <div className={Style.conclusionsActionWrapper}>
                            <Button category={ButtonCategory.FRONT_SHOP} text={sFront.enter}
                                    action={() => window.location.href = "/shop/box"}/>
                        </div>
                    </div>
                </div>
            </div>
            <Footer/>
        </Fragment>
    )
}

export default FrontPage