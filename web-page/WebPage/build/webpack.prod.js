const path = require('path');

const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = (environment) => {
    return {
        mode: 'production',
        entry: ['regenerator-runtime/runtime.js', path.resolve(__dirname, '..', `./src/index.js`)],
        output: {
            publicPath: `/`,
            path: path.resolve(__dirname, '..', `./dist`),
            filename: `js/[contenthash].js`
        },
        module: {
            rules: [
                {
                    test: /\.module\.(scss)$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                sourceMap: false
                            }
                        },
                        'postcss-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                implementation: require('node-sass'),
                                sourceMap: false
                            }
                        }
                    ]
                },
                {
                    test: /\.(scss)$/,
                    exclude: /\.module\.(scss)$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        {
                            loader: 'css-loader',
                            options: {
                                modules: false,
                                sourceMap: false
                            }
                        },
                        'postcss-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                implementation: require('node-sass'),
                                sourceMap: false
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new Dotenv({path: path.resolve(__dirname, '..', `./vars/.env.${environment}`)}),
            new HtmlWebpackPlugin({
                title: 'Step 2 - Era Zero Waste',
                template: path.resolve(__dirname, '..', './public/index.html'),
                filename: path.resolve(__dirname, '..', `./dist/index.html`),
                inject: true,
                minify: {
                    collapseWhitespace: true,
                    keepClosingSlash: true,
                    minifyCSS: true,
                    minifyJS: true,
                    minifyURLs: true,
                    removeComments: true,
                    removeRedundantAttributes: true,
                    removeEmptyAttributes: true,
                    removeStyleLinkTypeAttributes: true,
                    useShortDoctype: true
                }
            }),
            new MiniCssExtractPlugin({
                filename: 'css/[contenthash].css',
                chunkFilename: 'css/[contenthash].css'
            })
        ],
        optimization: {
            minimizer: [
                new CssMinimizerPlugin(),
                new TerserPlugin({
                    parallel: true,
                    terserOptions: {
                        format: {
                            comments: false,
                        }
                    },
                    extractComments: false
                })
            ]
        }
    };
};