const path = require('path');

const Dotenv = require('dotenv-webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (environment) => {
    return {
        mode: 'development',
        entry: ['regenerator-runtime/runtime.js', path.resolve(__dirname, '..', `./src/index.js`)],
        output: {
            publicPath: '/',
            filename: '[name].[contenthash].bundle.js'
        },
        devtool: 'eval-source-map',
        devServer: {
            host: 'localhost',
            port: 3000,
            hot: true,
            liveReload: true,
            historyApiFallback: true,
            open: true
        },
        module: {
            rules: [
                {
                    test: /\.module\.(scss)$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                sourceMap: true
                            }
                        },
                        'postcss-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                implementation: require('node-sass'),
                                sourceMap: true
                            }
                        }
                    ]
                },
                {
                    test: /\.(scss)$/,
                    exclude: /\.module\.(scss)$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                modules: false,
                                sourceMap: true
                            }
                        },
                        'postcss-loader',
                        {
                            loader: 'sass-loader',
                            options: {
                                implementation: require('node-sass'),
                                sourceMap: true
                            }
                        }
                    ]
                }
            ]
        },
        plugins: [
            new Dotenv({path: path.resolve(__dirname, '..', `./vars/.env.${environment}`)}),
            new HtmlWebpackPlugin({
                title: 'Test - Era Zero Waste',
                template: path.resolve(__dirname, '..', './public/index.html'),
                favicon: path.resolve(__dirname, '..', './public/favicon.ico'),
                inject: true
            })
        ]
    };
};