const SvgToMiniDataURI = require('mini-svg-data-uri');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

const browsers = ['Chrome >= 60', 'Safari >= 10.1', 'iOS >= 10.3', 'Firefox >= 54', 'Edge >= 15']

module.exports = {
    resolve: {extensions: ['.js', '.scss']},
    module: {
        rules: [
            {
                test: /\.(js)$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', {
                                modules: false,
                                useBuiltIns: false,
                                targets: {browsers: browsers}
                            }],
                            ['@babel/preset-react', {
                                modules: false,
                                useBuiltIns: false,
                                targets: {browsers: browsers}
                            }]
                        ],
                        plugins: ['@babel/plugin-syntax-dynamic-import', '@babel/plugin-proposal-class-properties']
                    }
                }
            },
            {
                test: /\.(svg)$/,
                exclude: /\.spec\.(svg)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            generator: content => SvgToMiniDataURI(content.toString()),
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpeg|webp)$/,
                use: [{loader: 'url-loader'}]
            }
        ]
    },
    plugins: [new CleanWebpackPlugin()]
};