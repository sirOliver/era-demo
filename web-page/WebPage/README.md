# Web-page

Deployment process:

1. Run `npm run build` to generate production build;
2. Move the contents of `dist` to S3 bucket used for static webpage hosting;
3. Move also `favicon.ico` to the same bucket.