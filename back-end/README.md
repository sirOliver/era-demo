# Back-end

Deployment process:

1. Build db docker: `docker build --tag [] ./db` (
   e.g., `docker build --tag registry.gitlab.com/siroliver/era-demo/db ./db`);
2. Build api docker: `docker build --tag [] ./Storage` (
   e.g., `docker build --tag registry.gitlab.com/siroliver/era-demo/storage ./Storage`);
3. Create a server virtual machine (in this example: "Amazon Linux 2 AMI (HVM) - Kernel 5.10, SSD Volume Type" with docker installed);
4. Deploy db, elastic search, and api:
    ```
    docker container run --publish 5432:5432 --detach --volume /home/ec2-user/postgresql:/var/lib/postgresql/data --name db registry.gitlab.com/siroliver/era-demo/db
    docker container run --publish 9200:9200 --detach --volume /home/ec2-user/elasticsearch:/usr/share/elasticsearch/data --name es --env "discovery.type=single-node" elasticsearch:6.8.23
    docker container run --network host --detach --name api --env PORT=8080 --env ORIGIN="http://localhost:3000" registry.gitlab.com/siroliver/era-demo/storage
    ```
5. Build parser for Google Spreadsheet document:
   ```
   docker build --tag build:exe --file ./Storage/PopulateDbBuild ./Storage
   docker container run --detach --name build build:exe
   docker cp build:/go/src/populateDb ./Storage/builds/populateDb
   docker rm -f build
   docker image rm build:exe
   ```
6. Build data migrator from db to elasticsearch in the same way;
7. Move populateDb and populateElastic to `/home/ec2-user/exe` and db.tsv to `/home/ec2-user` folders;
8. Execute populateDb first and then populateElastic from `/home/ec2-user/exe`.