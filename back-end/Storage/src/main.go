package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"storage/api"
)

func main() {
	log.Println(fmt.Sprintf("Listening on port %s", os.Getenv("PORT")))
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	http.HandleFunc("/departments", api.Departments)
	http.HandleFunc("/item/", api.Item)
	http.HandleFunc("/search", api.Search)
	log.Println(http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("PORT")), nil))
}
