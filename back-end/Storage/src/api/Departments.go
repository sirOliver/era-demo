package api

import (
	"encoding/json"
	"net/http"
	"os"
)

var departments = departmentsInit()

func departmentsInit() []string {
	var departments []string
	if rows, err := postgresClient.Query("SELECT unnest(enum_range(NULL::department_type))"); err != nil {
		panic(err)
	} else {
		defer rows.Close()
		for rows.Next() {
			var department string
			if err := rows.Scan(&department); err != nil {
				panic(err)
			} else {
				departments = append(departments, department)
			}
		}
	}
	return departments
}

func Departments(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Access-Control-Allow-Origin", os.Getenv("ORIGIN"))
	if responseRaw, err := json.Marshal(departments); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	} else if _, err := writer.Write(responseRaw); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	}
}
