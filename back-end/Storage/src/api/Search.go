package api

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/olivere/elastic"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"strings"
)

func Search(writer http.ResponseWriter, request *http.Request) {
	type Response struct {
		Count int64               `json:"count"`
		Items []ShortItemResponse `json:"items"`
	}
	writer.Header().Set("Access-Control-Allow-Origin", os.Getenv("ORIGIN"))
	if params, err := url.ParseQuery(request.URL.RawQuery); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	} else if offset, err := extractIntParam(params["offset"]); err != nil {
		writer.WriteHeader(http.StatusBadRequest)
	} else if categories, err := extractStringParam(params["categories"]); err != nil {
		writer.WriteHeader(http.StatusBadRequest)
	} else if query, err := extractStringParam(params["query"]); err != nil {
		writer.WriteHeader(http.StatusBadRequest)
	} else {
		var searchQuery elastic.Query
		if len(query) == 0 && len(categories) == 0 {
			searchQuery = elastic.NewMatchAllQuery()
		} else {
			boolQuery := elastic.NewBoolQuery()
			if len(query) > 0 {
				fuzzyQuery := elastic.NewBoolQuery()
				for _, word := range strings.Fields(query) {
					wordQuery := elastic.NewBoolQuery().
						Should(elastic.NewFuzzyQuery("label", word)).
						Should(elastic.NewFuzzyQuery("brand", word)).
						Should(elastic.NewFuzzyQuery("store", word)).
						Should(elastic.NewFuzzyQuery("description", word)).
						Should(elastic.NewFuzzyQuery("department", word)).
						Should(elastic.NewFuzzyQuery("use", word)).
						Should(elastic.NewFuzzyQuery("nutritional_value", word)).
						Should(elastic.NewFuzzyQuery("certifications", word)).
						Should(elastic.NewFuzzyQuery("packaging_extra_information", word)).
						Should(elastic.NewFuzzyQuery("packaging_material", word)).
						Should(elastic.NewFuzzyQuery("packaging_sustainability", word))
					fuzzyQuery.Must(wordQuery)
				}
				boolQuery.Must(fuzzyQuery)
			}
			if len(categories) > 0 {
				categoryQuery := elastic.NewBoolQuery()
				for _, category := range strings.Split(categories, ";") {
					categoryQuery.Should(elastic.NewMatchQuery("department", url.QueryEscape(category)))
				}
				boolQuery.Must(categoryQuery)
			}
			searchQuery = boolQuery
		}
		if searchResult, err := searchClient.Search().
			Index("shelf").
			Query(searchQuery).
			Sort("id", true).
			From(offset).
			Size(10).
			Do(context.Background()); err != nil {
			writer.WriteHeader(http.StatusFailedDependency)
		} else {
			response := Response{}
			for _, hit := range searchResult.Hits.Hits {
				var item ShortItemResponse
				if err := json.Unmarshal(*hit.Source, &item); err != nil {
					writer.WriteHeader(http.StatusFailedDependency)
				} else {
					response.Items = append(response.Items, item)
				}
			}
			response.Count = searchResult.Hits.TotalHits
			if response.Count < 1 {
				writer.WriteHeader(http.StatusNoContent)
			} else if responseRaw, err := json.Marshal(response); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
			} else if _, err := writer.Write(responseRaw); err != nil {
				writer.WriteHeader(http.StatusInternalServerError)
			}
		}
	}
}

func extractIntParam(params []string) (int, error) {
	if params != nil && len(params) > 0 {
		if value, err := strconv.Atoi(params[0]); err != nil {
			return 0, err
		} else {
			return value, nil
		}
	} else {
		return 0, fmt.Errorf("param not included")
	}
}

func extractStringParam(params []string) (string, error) {
	if params != nil && len(params) > 0 {
		if value, err := url.QueryUnescape(params[0]); err != nil {
			return "", nil
		} else {
			return value, nil
		}
	} else {
		return "", fmt.Errorf("param not included")
	}
}
