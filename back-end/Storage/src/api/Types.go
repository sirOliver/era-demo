package api

import (
	"database/sql"
	"github.com/google/uuid"
)

type ItemString sql.NullString
type ItemFloat sql.NullFloat64

type ShortItemResponse struct {
	Id                string   `json:"id"`
	Label             *string  `json:"label,omitempty"`
	Image             *string  `json:"image,omitempty"`
	PriceSaleValue    *float64 `json:"price_sale_value,omitempty"`
	PriceSaleCurrency *string  `json:"price_sale_currency,omitempty"`
}

type LongItemData struct {
	Id                        uuid.UUID
	Label                     sql.NullString
	Image                     sql.NullString
	PriceSaleValue            sql.NullFloat64
	PriceSaleCurrency         sql.NullString
	Brand                     sql.NullString
	Store                     sql.NullString
	Ean                       sql.NullString
	PfandEan                  sql.NullString
	Description               sql.NullString
	Department                sql.NullString
	CustomQuantity            bool
	Provenance                sql.NullString
	GpFaktor                  sql.NullFloat64
	Use                       sql.NullString
	NutritionalValue          sql.NullString
	ZeroWaste                 bool
	Vegan                     bool
	CertifiedOrganic          bool
	Certifications            sql.NullString
	PriceUnitValue            sql.NullFloat64
	PriceUnitCurrency         sql.NullString
	QuantityPerUnit           sql.NullString
	Unit                      sql.NullString
	Mwst                      sql.NullFloat64
	Stock                     sql.NullFloat64
	PackagingExtraInformation sql.NullString
	PackagingMaterial         sql.NullString
	PackagingSustainability   sql.NullString
}
