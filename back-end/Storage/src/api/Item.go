package api

import (
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	"net/http"
	"net/url"
	"os"
	"path"
	"strconv"
)

func Item(writer http.ResponseWriter, request *http.Request) {
	writer.Header().Set("Access-Control-Allow-Origin", os.Getenv("ORIGIN"))
	var data LongItemData
	_, suffix := path.Split(request.URL.Path)
	if id, err := uuid.Parse(suffix); err != nil {
		writer.WriteHeader(http.StatusBadRequest)
	} else if err := postgresClient.QueryRow("SELECT Brand.label, Store.label, "+
		"Item.ean, Item.pfand_ean, Item.label, Item.description, Item.department, Item.custom_quantity, "+
		"Item.provenance, Item.gp_faktor, Item.use, Item.nutritional_value, Item.zero_waste, Item.vegan, "+
		"Item.certified_organic, Item.certifications, Item.image, "+
		"Item.price_sale_value, Item.price_sale_currency, Item.price_unit_value, Item.price_unit_currency, "+
		"Item.quantity_per_unit, Item.unit, Item.mwst, Item.stock, "+
		"Item.packaging_extra_information, Item.packaging_material, Item.packaging_sustainability FROM Item "+
		"INNER JOIN Brand ON Brand.id=Item.brand_id "+
		"INNER JOIN Store ON Store.id=Item.store_id "+
		"WHERE Item.id=$1 AND Item.visible=true", id).Scan(&data.Brand, &data.Store,
		&data.Ean, &data.PfandEan, &data.Label, &data.Description, &data.Department, &data.CustomQuantity,
		&data.Provenance, &data.GpFaktor, &data.Use, &data.NutritionalValue, &data.ZeroWaste, &data.Vegan,
		&data.CertifiedOrganic, &data.Certifications, &data.Image,
		&data.PriceSaleValue, &data.PriceSaleCurrency, &data.PriceUnitValue, &data.PriceUnitCurrency,
		&data.QuantityPerUnit, &data.Unit, &data.Mwst, &data.Stock,
		&data.PackagingExtraInformation, &data.PackagingMaterial, &data.PackagingSustainability); err != nil {
		if err == sql.ErrNoRows {
			writer.WriteHeader(http.StatusNoContent)
		} else {
			writer.WriteHeader(http.StatusFailedDependency)
		}
	} else if responseRaw, err := MarshalJSON(data); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	} else if _, err := writer.Write(responseRaw); err != nil {
		writer.WriteHeader(http.StatusInternalServerError)
	}
}

func MarshalJSON(i LongItemData) ([]byte, error) {
	content := ""
	content += "\"id\":\"" + i.Id.String() + "\","
	if i.Brand.Valid {
		content += "\"brand\":\"" + url.QueryEscape(i.Brand.String) + "\","
	}
	if i.Store.Valid {
		content += "\"store\":\"" + url.QueryEscape(i.Store.String) + "\","
	}
	if i.Ean.Valid {
		content += "\"ean\":\"" + url.QueryEscape(i.Ean.String) + "\","
	}
	if i.PfandEan.Valid {
		content += "\"pfand_ean\":\"" + url.QueryEscape(i.PfandEan.String) + "\","
	}
	if i.Label.Valid {
		content += "\"label\":\"" + url.QueryEscape(i.Label.String) + "\","
	}
	if i.Description.Valid {
		content += "\"description\":\"" + url.QueryEscape(i.Description.String) + "\","
	}
	if i.Department.Valid {
		content += "\"department\":\"" + url.QueryEscape(i.Department.String) + "\","
	}
	content += "\"custom_quantity\":" + strconv.FormatBool(i.CustomQuantity) + ","
	if i.Provenance.Valid {
		content += "\"provenance\":\"" + url.QueryEscape(i.Provenance.String) + "\","
	}
	if i.GpFaktor.Valid {
		content += "\"gp_faktor\":" + fmt.Sprintf("%f", i.GpFaktor.Float64) + ","
	}
	if i.Use.Valid {
		content += "\"use\":\"" + url.QueryEscape(i.Use.String) + "\","
	}
	if i.NutritionalValue.Valid {
		content += "\"nutritional_value\":\"" + url.QueryEscape(i.NutritionalValue.String) + "\","
	}
	content += "\"zero_waste\":" + strconv.FormatBool(i.ZeroWaste) + ","
	content += "\"vegan\":" + strconv.FormatBool(i.Vegan) + ","
	content += "\"certified_organic\":" + strconv.FormatBool(i.CertifiedOrganic) + ","
	if i.Certifications.Valid {
		content += "\"certifications\":\"" + url.QueryEscape(i.Certifications.String) + "\","
	}
	if i.Image.Valid {
		content += "\"image\":\"" + i.Image.String + "\","
	}
	if i.PriceSaleValue.Valid {
		content += "\"price_sale_value\":" + fmt.Sprintf("%f", i.PriceSaleValue.Float64) + ","
	}
	if i.PriceSaleCurrency.Valid {
		content += "\"price_sale_currency\":\"" + url.QueryEscape(i.PriceSaleCurrency.String) + "\","
	}
	if i.PriceUnitValue.Valid {
		content += "\"price_unit_value\":" + fmt.Sprintf("%f", i.PriceUnitValue.Float64) + ","
	}
	if i.PriceUnitCurrency.Valid {
		content += "\"price_unit_currency\":\"" + url.QueryEscape(i.PriceUnitCurrency.String) + "\","
	}
	if i.QuantityPerUnit.Valid {
		content += "\"quantity_per_unit\":\"" + url.QueryEscape(i.QuantityPerUnit.String) + "\","
	}
	if i.Unit.Valid {
		content += "\"unit\":\"" + url.QueryEscape(i.Unit.String) + "\","
	}
	if i.Mwst.Valid {
		content += "\"mwst\":" + fmt.Sprintf("%f", i.Mwst.Float64) + ","
	}
	if i.Stock.Valid {
		content += "\"stock\":" + fmt.Sprintf("%f", i.Stock.Float64) + ","
	}
	if i.PackagingExtraInformation.Valid {
		content += "\"packaging_extra_information\":\"" + url.QueryEscape(i.PackagingExtraInformation.String) + "\","
	}
	if i.PackagingMaterial.Valid {
		content += "\"packaging_material\":\"" + url.QueryEscape(i.PackagingMaterial.String) + "\","
	}
	if i.PackagingSustainability.Valid {
		content += "\"packaging_sustainability\":\"" + url.QueryEscape(i.PackagingSustainability.String) + "\","
	}
	return []byte("{" + content[0:len(content)-1] + "}"), nil
}
