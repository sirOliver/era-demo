package api

import (
	"database/sql"
	_ "github.com/lib/pq"
	"github.com/olivere/elastic"
)

var postgresClient = postgresClientInit()

func postgresClientInit() *sql.DB {
	client, err := sql.Open("postgres", "host=localhost port=5432 user=postgres dbname=era sslmode=disable")
	if err != nil {
		panic(err)
	}
	return client
}

var searchClient = searchClientInit()

func searchClientInit() *elastic.Client {
	client, err := elastic.NewClient()
	if err != nil {
		panic(err)
	}
	return client
}
