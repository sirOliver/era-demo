package main

import (
	"bufio"
	"database/sql"
	"fmt"
	"github.com/google/uuid"
	_ "github.com/lib/pq"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	client, err := sql.Open("postgres", "host=localhost port=5432 user=postgres dbname=era sslmode=disable")
	if err != nil {
		panic(err)
	}
	defer client.Close()

	file, err := os.Open("../db.tsv")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		values := strings.Split(scanner.Text(), "\t")
		ean := strings.TrimSpace(values[0])
		provenance := strings.TrimSpace(values[1])
		brand := strings.TrimSpace(values[2])
		perWeight := strings.TrimSpace(values[3])
		gpFaktor := strings.TrimSpace(values[4])
		stock := strings.TrimSpace(values[5])
		visible := strings.TrimSpace(values[6])
		name := strings.TrimSpace(values[7])
		description := strings.TrimSpace(values[8])
		certifications := strings.TrimSpace(values[9])
		priceOfSale := strings.TrimSpace(values[10])
		quantityPerUnit := strings.TrimSpace(values[11])
		pricePerUnit := strings.TrimSpace(values[12])
		unit := strings.TrimSpace(values[13])
		mwst := strings.TrimSpace(values[14])
		pfandEan := strings.TrimSpace(values[15])
		zeroWaste := strings.TrimSpace(values[16])
		image := strings.TrimSpace(values[17])
		department := strings.TrimSpace(values[18])
		nutritionalValue := strings.TrimSpace(values[19])
		certifiedOrganic := strings.TrimSpace(values[20])
		packagingExtraInformation := strings.TrimSpace(values[21])
		packagingMaterial := strings.TrimSpace(values[22])
		packagingSustainability := strings.TrimSpace(values[23])
		store := strings.TrimSpace(values[25])
		usage := strings.TrimSpace(values[26])
		vegan := strings.TrimSpace(values[27])

		brandLabel := sql.NullString{String: brand, Valid: !(len(brand) < 1 || brand == "#N/A")}
		brandId := insertUniqueLabel(client, "Brand", brandLabel)
		storeLabel := sql.NullString{String: store, Valid: !(len(store) < 1)}
		storeId := insertUniqueLabel(client, "Store", storeLabel)
		eanValue := stringOrNull(ean)
		pfandEanValue := stringOrNull(pfandEan)
		labelValue := stringOrNull(name)
		descriptionValue := stringOrNull(description)
		departmentValue := stringOrNull(department)
		customQuantityValue := boolean(perWeight)
		provenanceValue := stringOrNull(provenance)
		gpFaktorValue := floatOrNull(gpFaktor)
		useValue := stringOrNull(usage)
		nutritionalValueValue := stringOrNull(nutritionalValue)
		zeroWasteValue := boolean(zeroWaste)
		veganValue := boolean(vegan)
		certifiedOrganicValue := boolean(certifiedOrganic)
		certificationsValue := stringOrNull(certifications)
		imageValue := stringOrNull(image)
		priceSaleValue := floatOrNull(strings.ReplaceAll(strings.ReplaceAll(priceOfSale, "€", ""), ",", ""))
		if len(priceOfSale) > 0 && !strings.Contains(priceOfSale, "€") {
			log.Println(fmt.Sprintf("no eur sign: '%s'", priceOfSale))
		}
		priceSaleCurrency := "EUR"
		priceUnitValue := floatOrNull(strings.ReplaceAll(strings.ReplaceAll(pricePerUnit, "€", ""), ",", ""))
		if len(priceOfSale) > 0 && !strings.Contains(pricePerUnit, "€") {
			log.Println(fmt.Sprintf("no eur sign: '%s'", priceOfSale)) //todo line 2246 looks very strange
		}
		priceUnitCurrency := "EUR"
		quantityPerUnitValue := stringOrNull(quantityPerUnit)
		unitValue := stringOrNull(unit)
		mwstValue := floatOrNull(mwst)
		stockValue := floatOrNull(stock)
		visibleValue := boolean(visible)
		packagingExtraInformationValue := stringOrNull(packagingExtraInformation)
		packagingMaterialValue := stringOrNull(packagingMaterial)
		packagingSustainabilityValue := stringOrNull(packagingSustainability)

		itemId := uuid.New()
		_, err = client.Exec("INSERT INTO Item (id, brand_id, store_id, ean, pfand_ean, label, description, department, custom_quantity, provenance, gp_faktor, use, nutritional_value, zero_waste, vegan, certified_organic, certifications, image, price_sale_value, price_sale_currency, price_unit_value, price_unit_currency, quantity_per_unit, unit, mwst, stock, visible, packaging_extra_information, packaging_material, packaging_sustainability) "+
			"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24, $25, $26, $27, $28, $29, $30)",
			itemId, brandId, storeId, eanValue, pfandEanValue, labelValue, descriptionValue, departmentValue, customQuantityValue, provenanceValue, gpFaktorValue, useValue, nutritionalValueValue, zeroWasteValue, veganValue, certifiedOrganicValue, certificationsValue, imageValue, priceSaleValue, priceSaleCurrency, priceUnitValue, priceUnitCurrency, quantityPerUnitValue, unitValue, mwstValue, stockValue, visibleValue, packagingExtraInformationValue, packagingMaterialValue, packagingSustainabilityValue)
		if err != nil {
			panic(err)
		}
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}
}

func insertUniqueLabel(client *sql.DB, table string, label sql.NullString) uint64 {
	var id uint64
	query := fmt.Sprintf("SELECT id FROM %s WHERE label IS NULL", table)
	insert := fmt.Sprintf("INSERT INTO %s (label) VALUES (NULL) RETURNING id", table)
	var params []interface{}
	if label.Valid {
		query = fmt.Sprintf("SELECT id FROM %s WHERE label=$1", table)
		insert = fmt.Sprintf("INSERT INTO %s (label) VALUES ($1) RETURNING id", table)
		params = []interface{}{label.String}
	}
	if err := client.QueryRow(query, params...).Scan(&id); err != nil && err != sql.ErrNoRows {
		panic(err)
	} else if err == sql.ErrNoRows {
		if err := client.QueryRow(insert, params...).Scan(&id); err != nil {
			panic(err)
		}
	}
	return id
}

func stringOrNull(value string) sql.NullString {
	if len(value) < 1 {
		return sql.NullString{}
	} else {
		return sql.NullString{String: strings.TrimSpace(value), Valid: true}
	}
}

func floatOrNull(value string) sql.NullFloat64 {
	if len(value) < 1 {
		return sql.NullFloat64{}
	} else if float, err := strconv.ParseFloat(strings.TrimSpace(value), 64); err != nil {
		panic(err)
	} else {
		return sql.NullFloat64{Float64: float, Valid: true}
	}
}

func boolean(value string) bool {
	return value == "yes"
}
