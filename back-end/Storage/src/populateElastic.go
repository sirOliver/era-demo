package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"github.com/olivere/elastic"
	"log"
	"storage/api"
)

const mapping = `
{
	"settings":{
		"number_of_shards": 1,
		"number_of_replicas": 0
	},
	"mappings":{
		"item":{
			"properties":{
				"id":{
					"type":"keyword"
				},
				"label":{
					"type":"text"
				},
				"brand":{
					"type":"text"
				},
				"store":{
					"type":"text"
				},
				"description":{
					"type":"text"
				},
				"department":{
					"type":"keyword"
				},
				"use":{
					"type":"text"
				},
				"nutritional_value":{
					"type":"text"
				},
				"certifications":{
					"type":"text"
				},
				"packaging_extra_information":{
					"type":"text"
				},
				"packaging_material":{
					"type":"text"
				},
				"packaging_sustainability":{
					"type":"text"
				}
			}
		}
	}
}`

func main() {
	postgresClient, err := sql.Open("postgres", "host=localhost port=5432 user=postgres dbname=era sslmode=disable")
	if err != nil {
		panic(err)
	}
	defer postgresClient.Close()

	ctx := context.Background()
	searchClient, err := elastic.NewClient()
	if err != nil {
		panic(err)
	}

	_, code, err := searchClient.Ping("http://localhost:9200").Do(ctx)
	if err != nil {
		panic(err)
	}
	log.Println(fmt.Sprintf("Elasticsearch returned %d", code))

	exists, err := searchClient.IndexExists("shelf").Do(ctx)
	if err != nil {
		panic(err)
	}
	if !exists {
		if _, err := searchClient.CreateIndex("shelf").BodyString(mapping).Do(ctx); err != nil {
			panic(err)
		}
	}

	if rows, err := postgresClient.Query("SELECT Item.id, Brand.label, Store.label, " +
		"Item.label, Item.description, Item.department, Item.use, Item.nutritional_value, " +
		"Item.certifications, Item.image, Item.price_sale_value, Item.price_sale_currency, " +
		"Item.packaging_extra_information, Item.packaging_material, Item.packaging_sustainability FROM Item " +
		"INNER JOIN Brand ON Brand.id=Item.brand_id " +
		"INNER JOIN Store ON Store.id=Item.store_id " +
		"WHERE Item.visible=true"); err != nil {
		panic(err)
	} else {
		defer rows.Close()
		for rows.Next() {
			var item api.LongItemData
			if err := rows.Scan(&item.Id, &item.Brand, &item.Store, &item.Label, &item.Description, &item.Department, &item.Use, &item.NutritionalValue,
				&item.Certifications, &item.Image, &item.PriceSaleValue, &item.PriceSaleCurrency, &item.PackagingExtraInformation, &item.PackagingMaterial, &item.PackagingSustainability); err != nil {
				panic(err)
			} else if raw, err := api.MarshalJSON(item); err != nil {
				panic(err)
			} else {
				if _, err := searchClient.Index().
					Index("shelf").
					Type("item").
					Id(item.Id.String()).
					BodyString(string(raw)).
					Do(ctx); err != nil {
					log.Println(string(raw))
					panic(err)
				}
			}
		}
		if _, err = searchClient.Flush().Index("shelf").Do(ctx); err != nil {
			panic(err)
		}
	}
}
