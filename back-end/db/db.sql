CREATE DATABASE era;

\c era

CREATE TYPE department_type AS ENUM ('Self-Care', 'Home-Care', 'Condiments', 'Beverages', 'Fruits & Vegetables', 'Coffee & Tea', 'Seeds & Nuts', 'Sweets', 'Cereals & Grains', 'Dry/Baking Goods');

CREATE TYPE currency_type AS ENUM ('EUR');

CREATE TABLE IF NOT EXISTS Brand
(
    id      serial primary key,
    label   text unique
);

CREATE TABLE IF NOT EXISTS Store
(
    id      serial primary key,
    label   text unique
);

CREATE TABLE IF NOT EXISTS Item
(
    id                              uuid primary key,
    brand_id                        integer references Brand (id) not null,
    store_id                        integer references Store (id) not null,
    ean                             text,
    pfand_ean                       text,
    label                           text,
    description                     text,
    department                      department_type,
    custom_quantity                 boolean default false,
    provenance                      text,
    gp_faktor                       real,
    use                             text,
    nutritional_value               text,
    zero_waste                      boolean default false,
    vegan                           boolean default false,
    certified_organic               boolean default false,
    certifications                  text,
    image                           text,
    price_sale_value                real,
    price_sale_currency             currency_type,
    price_unit_value                real,
    price_unit_currency             currency_type,
    quantity_per_unit               text,
    unit                            text,
    mwst                            real,
    stock                           real,
    visible                         boolean default false,
    packaging_extra_information     text,
    packaging_material              text,
    packaging_sustainability        text
);
